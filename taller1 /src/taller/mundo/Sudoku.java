package taller.mundo;

/*
 * Sudoku.java
 * This file is part of SudokuCLI
 *
 * Copyright (C) 2016 - ISIS1206 Team
 *
 * SudokuCLI is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * SudokuCLI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SudokuCLI. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *  La clase <tt>Sudoku</tt> abstrae el funcionamiento de un juego de Sudoku,
 *  a partir de la definición de un tablero de juego, una solución posible, así
 *  como funciones que permiten realizar cambios sobre el tablero y la verificación
 *  de la validez del mismo durante cada etapa de juego.
 *  @author ISIS1206 Team
 */

public class Sudoku
{
    /**
     * Enumeración que describe el nivel de dificultad de un juego de sudoku,
     * dificultad comprendida como el número inicial de ayudas del tablero en
     * cuestión.
     **/
    public static enum Rating
    {
        /**
         * Dificultad "Infantil", Número de ayudas: 50.
         **/
        INFANTIL(50, "Infantil"),
        /**
         * Dificultad "Normal", Número de ayudas: 40.
         **/
        NORMAL(40, "Normal"),
        /**
         * Dificultad "Difícil", Número de ayudas: 34.
         **/
        DIFICIL(34, "Difícil"),
        /**
         * Dificultad "Maquiavélico", Número de ayudas: 26.
         **/
        MAQUIAVELICO(26, "Maquiavélico"),
        /**
         * Dificultad "Diabólico", Número de ayudas: 17.
         **/
        DIABOLICO(17, "Diabólico");

        private final int numGivens;
        private final String name;

        Rating(int g, String nameN)
        {
            numGivens = g;
            name = nameN;
            
        }

        int getNumGivens()
        {
            return numGivens;
        }

        @Override
        public String toString()
        {
            return name;
        }
    }

    /**
     * Matriz que describe el estado actual del juego de Sudoku.
     * Cada entrada de la matriz, contiene un número entre 1 y 9 si ésta no se
     * encuentra vacía. 0 de lo contrario
     **/
    private int[][] board;
    /**
     * Matriz que describe una solución del juego de Sudoku,
     * cada entrada contiene un número que se encuentra en el rango [1, 9].
     **/
    private int[][] solution;
    /**
     * Matriz que describe el estado inicial del juego de Sudoku.
     * Cada entrada de la matriz, contiene un número entre 1 y 9 si ésta no se
     * encuentra vacía. 0 de lo contrario
     **/
    private int[][] initialBoard;


    /**
     * Reinicia el estado actual del juego y genera un nuevo tablero completo.
     **/
    public void resetGame()
    {
        solution = SudokuLib.simulated_annealing(10000, false);
    }

    /**
     * Configura un nuevo juego de sudoku, sujeto a un nivel de dificultad dado.
     *
     * @param r Nivel de dificultad del juego
     **/
    public void setUpGame(Rating r)
    {
         board = SudokuLib.getSudokuBoard(solution, r.getNumGivens());
         initialBoard = new int[9][];
         for(int i = 0; i < 9; i++)
         {
              int[] row = new int[9];
              initialBoard[i] = row;
              for(int j = 0; j < 9; j++)
              {
                  initialBoard[i][j] = board[i][j];
              }
         }
    }

    /**
     * Retorna una solución posible a la partida actual
     *
     * @return Matriz que describe una solución del juego de Sudoku,
     * cada entrada contiene un número que se encuentra en el rango [1, 9].
     **/
    public int[][] getSolution()
    {
        return solution;
    }

    /**
     * Retorna el estado actual del tablero de Sudoku.
     *
     * @return Matriz que describe el estado actual del juego de Sudoku.
     * Cada entrada de la matriz, contiene un número en el rango [1, 9], si ésta no se
     * encuentra vacía. 0 de lo contrario
     **/
    public int[][] getBoard()
    {
        return board;
    }

    /**
     * Establece si el tablero actual se encuentra completo, y corresponde a una solución
     * posible del tablero inicial.
     *
     * @return true si el juego ha finalizado, false de lo contrario.
     **/
    public boolean hasGameFinished()
    {
         return SudokuLib.evalConstraints(board) == 0;
    }

    /**
     * Reemplaza la entrada de la casilla situada en la coordenada row, col por el número num.
     * Tras realizar la sustitución, se procede a evaluar la integridad del tablero. Si el número
     * introducido viola alguna de las restricciones de juego, se emite un mensaje y el número no 
     * es reemplazado. De igual forma, si se solicita la sustitución de una casilla pista, se emite
     * un mensaje indicando la invalidez de la solicitud.
     *
     * @param row Número entero que describe la fila en la cual se encuentra la 
     * casilla a reemplazar. 0 <= row < 9
     * @param col Número entero que describe la columna en la cual se encuentra la 
     * casilla a reemplazar. 0 <= col < 9
     * @param num Número a reemplazar en la casilla solicitada 
     **/
    public String replaceValue(int row, int col, int num, boolean deletion)
    {
         String result = "";
         if(initialBoard[row][col] != 0)
         {
             result = "No es posible "+(deletion ? "eliminar" : "reemplazar")+" el valor de una casilla pista.";
         }
         else
         {
             int lastValue = board[row][col];
             board[row][col] = num;
             result = SudokuLib.checkConstraintsEntry(board, row, col);
             if(result.length() > 0)
             {
                  board[row][col] = lastValue;
                  result = "El número introducido ("+num+"), se encuentra en las siguientes casillas: "+result;
             }
             else
             {
                  if(!deletion)
                  {
                      result = "El número ("+num+") se ha introducido satisfactoriamente.";
                  }
                  else
                  {
                      result = "La casilla ahora se encuenta vacía.";
                  }
             }
         }
         return result;
    }
}
